﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFOpenFileDialog
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /**
         * @brief 開くボタンが押下された時に呼び出されます。
         *
         * @param [in] sender 開くボタン
         * @param [in] e イベント
         */
        private void OpenFileButton_Click(object sender, RoutedEventArgs e)
        {
            // ファイルを開くダイアログを生成します。
            var dialog = new OpenFileDialog();

            // フィルターを設定します。
            // この設定は任意です。
            dialog.Filter = "テキストファイル(*.txt)|*.txt|CSVファイル(*.csv)|*.csv|全てのファイル(*.*)|*.*";

            // ファイルを開くダイアログを表示します。
            var result = dialog.ShowDialog() ?? false;

            // 開くボタン以外が押下された場合
            if (!result)
            {
                // 終了します。
                return;
            }

            // ファイルを開くダイアログで選択されたファイルパス名を表示します。
            MessageBox.Show(dialog.FileName);
        }
    }
}
